import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from sklearn import metrics
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import GridSearchCV
from sklearn import tree
import pydotplus
from IPython.display import Image, display
from sklearn.tree import export_graphviz
import os

plt.style.use({'figure.figsize': (20, 16)})
plt.style.use('seaborn-white')
plt.rcParams['font.sans-serif'] = ['SimHei']
plt.rcParams['axes.unicode_minus'] = False

house = pd.read_csv('housing_data.csv', sep='\s+', encoding='gbk')
feature = house.drop(["medv"], axis=1)
label = house["medv"]
x_xunlian, x_ceshi, y_xunlian, y_ceshi = train_test_split(feature, label , random_state=0)

linRegression=LinearRegression()
linRegression.fit(x_xunlian,y_xunlian)
linRegression.score(x_ceshi,y_ceshi)
print("个特征与标签线性回归后斜率：",linRegression.coef_)
plt.title('个特征与标签回归后斜率')
plt.plot(linRegression.coef_)
plt.show()



#线性回归误差估算
pre=linRegression.predict(x_ceshi)
departure=pre-y_ceshi
print("线性回归均方根误差：",np.sum(np.sqrt(departure*departure))/102)

pg = {
    'n_estimators': [5, 10, 20, 50, 100, 200],
    'max_depth': [3, 5, 7],
    'max_features': [0.6, 0.7, 0.8, 1]
}

randomforest = RandomForestRegressor()

grid = GridSearchCV(randomforest, param_grid=pg, cv=3)
grid.fit(x_xunlian, y_xunlian)


forestRegression = grid.best_estimator_
print("效果最好模型：", forestRegression, '\n')




os.environ["PATH"] += os.pathsep + 'D:/Graphviz/bin'
estimator = forestRegression.estimators_[3]
datadot = tree.export_graphviz(estimator,
                               out_file=None,
                               filled=True,
                               rounded=True
                               )
graph = pydotplus.graph_from_dot_data(datadot)
Image(graph.create_png())

print("各参数重要度指标：", forestRegression.feature_importances_, '\n')




MeanSquareError = metrics.mean_squared_error(label , forestRegression.predict(feature))
RMeanSquareError = np.sqrt(MeanSquareError)
print('均方误差=%.16f,均方根误差=%.16f' % (MeanSquareError, RMeanSquareError))


#将回归结果导入csv文件
prediction = {"forecast": forestRegression.predict(x_ceshi)}
prediction = pd.DataFrame(prediction)
prediction.to_csv('波士顿房价数据预测.csv')

#显示回归预测结果前五行
outcome = {"labels": y_ceshi, "forecast": forestRegression.predict(x_ceshi)}
outcome = pd.DataFrame(outcome)
print("回归预测结果：\n", outcome.head(), '\n')

print('特征重要度排序：')
names = feature.columns
importanceOfFeature = forestRegression.feature_importances_
indexes = np.argsort(importanceOfFeature)

for i in indexes:
    print('%s (%f)' % (names[i], importanceOfFeature[i]))


#预测结果可视化
outcome['labels'].plot(style='k.', figsize=(15, 5))
outcome['forecast'].plot(style='r.')
plt.title("预测房价与实际房价对比图")
plt.legend(fontsize=15, markerscale=3)
plt.tick_params(labelsize=15)
plt.grid()


#特征重要度可视化
plt.figure(figsize=(7, 5))
plt.title('各参数重要程度指数')
plt.bar(range(len(importanceOfFeature)), importanceOfFeature[indexes], color='red')
plt.xticks(range(len(importanceOfFeature)), np.array(names)[indexes], color='black')
plt.show()

